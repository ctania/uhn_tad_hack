<?php

namespace App\Repository;

use App\Entity\Especialista;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Especialista|null find($id, $lockMode = null, $lockVersion = null)
 * @method Especialista|null findOneBy(array $criteria, array $orderBy = null)
 * @method Especialista[]    findAll()
 * @method Especialista[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EspecialistaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Especialista::class);
    }

    // /**
    //  * @return Especialista[] Returns an array of Especialista objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Especialista
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
