<?php

namespace App\Repository;

use App\Entity\ProfesionEspecialista;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ProfesionEspecialista|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProfesionEspecialista|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProfesionEspecialista[]    findAll()
 * @method ProfesionEspecialista[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfesionEspecialistaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProfesionEspecialista::class);
    }

    // /**
    //  * @return ProfesionEspecialista[] Returns an array of ProfesionEspecialista objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProfesionEspecialista
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
