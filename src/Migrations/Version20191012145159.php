<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191012145159 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE adjunto (id INT AUTO_INCREMENT NOT NULL, respuesta_id INT DEFAULT NULL, solicitud_id INT DEFAULT NULL, uri VARCHAR(255) NOT NULL, nombre VARCHAR(255) NOT NULL, INDEX IDX_78BCCC30D9BA57A2 (respuesta_id), INDEX IDX_78BCCC301CB9D6E4 (solicitud_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE area (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, codigo VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE area_profesion (area_id INT NOT NULL, profesion_id INT NOT NULL, INDEX IDX_E9C1E8D6BD0F409C (area_id), INDEX IDX_E9C1E8D6C5AF4D0F (profesion_id), PRIMARY KEY(area_id, profesion_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE especialista (id INT AUTO_INCREMENT NOT NULL, idioma_id INT NOT NULL, telefono VARCHAR(50) NOT NULL, nombres VARCHAR(255) NOT NULL, apellidos VARCHAR(255) NOT NULL, tipo_identificacion VARCHAR(10) NOT NULL, identificacion VARCHAR(50) NOT NULL, INDEX IDX_F206C397DEDC0611 (idioma_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE idioma (id INT AUTO_INCREMENT NOT NULL, codigo VARCHAR(10) NOT NULL, nombre VARCHAR(30) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE plan (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, maximo_solicitudes_semanales INT NOT NULL, maximo_respuestas_solicitud INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profesion (id INT AUTO_INCREMENT NOT NULL, nombre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profesion_especialista (id INT AUTO_INCREMENT NOT NULL, especialista_id INT NOT NULL, profesion_id INT NOT NULL, numero_tarjeta_profesional VARCHAR(255) NOT NULL, universidad VARCHAR(255) NOT NULL, titulo_universitario VARCHAR(255) NOT NULL, fecha_grado DATE NOT NULL, INDEX IDX_C31247C97A94A0F (especialista_id), INDEX IDX_C31247CC5AF4D0F (profesion_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE respuesta (id INT AUTO_INCREMENT NOT NULL, especialista_id INT NOT NULL, solicitud_id INT NOT NULL, respuesta VARCHAR(2000) NOT NULL, fecha DATE NOT NULL, INDEX IDX_6C6EC5EE97A94A0F (especialista_id), INDEX IDX_6C6EC5EE1CB9D6E4 (solicitud_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE solicitud (id INT AUTO_INCREMENT NOT NULL, idioma_id INT NOT NULL, area_id INT NOT NULL, usuario_id INT NOT NULL, codigo VARCHAR(50) NOT NULL, asunto VARCHAR(125) NOT NULL, descripcion VARCHAR(2000) NOT NULL, fecha DATE NOT NULL, prioridad INT NOT NULL, es_activa TINYINT(1) NOT NULL, respuesta_sms TINYINT(1) NOT NULL, INDEX IDX_96D27CC0DEDC0611 (idioma_id), INDEX IDX_96D27CC0BD0F409C (area_id), INDEX IDX_96D27CC0DB38439E (usuario_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE usuario (id INT AUTO_INCREMENT NOT NULL, plan_id INT NOT NULL, telefono VARCHAR(50) NOT NULL, nombres VARCHAR(255) NOT NULL, apellidos VARCHAR(255) NOT NULL, tipo_identificacion VARCHAR(10) NOT NULL, identificacion VARCHAR(100) NOT NULL, INDEX IDX_2265B05DE899029B (plan_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE adjunto ADD CONSTRAINT FK_78BCCC30D9BA57A2 FOREIGN KEY (respuesta_id) REFERENCES respuesta (id)');
        $this->addSql('ALTER TABLE adjunto ADD CONSTRAINT FK_78BCCC301CB9D6E4 FOREIGN KEY (solicitud_id) REFERENCES solicitud (id)');
        $this->addSql('ALTER TABLE area_profesion ADD CONSTRAINT FK_E9C1E8D6BD0F409C FOREIGN KEY (area_id) REFERENCES area (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE area_profesion ADD CONSTRAINT FK_E9C1E8D6C5AF4D0F FOREIGN KEY (profesion_id) REFERENCES profesion (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE especialista ADD CONSTRAINT FK_F206C397DEDC0611 FOREIGN KEY (idioma_id) REFERENCES idioma (id)');
        $this->addSql('ALTER TABLE profesion_especialista ADD CONSTRAINT FK_C31247C97A94A0F FOREIGN KEY (especialista_id) REFERENCES especialista (id)');
        $this->addSql('ALTER TABLE profesion_especialista ADD CONSTRAINT FK_C31247CC5AF4D0F FOREIGN KEY (profesion_id) REFERENCES profesion (id)');
        $this->addSql('ALTER TABLE respuesta ADD CONSTRAINT FK_6C6EC5EE97A94A0F FOREIGN KEY (especialista_id) REFERENCES especialista (id)');
        $this->addSql('ALTER TABLE respuesta ADD CONSTRAINT FK_6C6EC5EE1CB9D6E4 FOREIGN KEY (solicitud_id) REFERENCES solicitud (id)');
        $this->addSql('ALTER TABLE solicitud ADD CONSTRAINT FK_96D27CC0DEDC0611 FOREIGN KEY (idioma_id) REFERENCES idioma (id)');
        $this->addSql('ALTER TABLE solicitud ADD CONSTRAINT FK_96D27CC0BD0F409C FOREIGN KEY (area_id) REFERENCES area (id)');
        $this->addSql('ALTER TABLE solicitud ADD CONSTRAINT FK_96D27CC0DB38439E FOREIGN KEY (usuario_id) REFERENCES usuario (id)');
        $this->addSql('ALTER TABLE usuario ADD CONSTRAINT FK_2265B05DE899029B FOREIGN KEY (plan_id) REFERENCES plan (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE area_profesion DROP FOREIGN KEY FK_E9C1E8D6BD0F409C');
        $this->addSql('ALTER TABLE solicitud DROP FOREIGN KEY FK_96D27CC0BD0F409C');
        $this->addSql('ALTER TABLE profesion_especialista DROP FOREIGN KEY FK_C31247C97A94A0F');
        $this->addSql('ALTER TABLE respuesta DROP FOREIGN KEY FK_6C6EC5EE97A94A0F');
        $this->addSql('ALTER TABLE especialista DROP FOREIGN KEY FK_F206C397DEDC0611');
        $this->addSql('ALTER TABLE solicitud DROP FOREIGN KEY FK_96D27CC0DEDC0611');
        $this->addSql('ALTER TABLE usuario DROP FOREIGN KEY FK_2265B05DE899029B');
        $this->addSql('ALTER TABLE area_profesion DROP FOREIGN KEY FK_E9C1E8D6C5AF4D0F');
        $this->addSql('ALTER TABLE profesion_especialista DROP FOREIGN KEY FK_C31247CC5AF4D0F');
        $this->addSql('ALTER TABLE adjunto DROP FOREIGN KEY FK_78BCCC30D9BA57A2');
        $this->addSql('ALTER TABLE adjunto DROP FOREIGN KEY FK_78BCCC301CB9D6E4');
        $this->addSql('ALTER TABLE respuesta DROP FOREIGN KEY FK_6C6EC5EE1CB9D6E4');
        $this->addSql('ALTER TABLE solicitud DROP FOREIGN KEY FK_96D27CC0DB38439E');
        $this->addSql('DROP TABLE adjunto');
        $this->addSql('DROP TABLE area');
        $this->addSql('DROP TABLE area_profesion');
        $this->addSql('DROP TABLE especialista');
        $this->addSql('DROP TABLE idioma');
        $this->addSql('DROP TABLE plan');
        $this->addSql('DROP TABLE profesion');
        $this->addSql('DROP TABLE profesion_especialista');
        $this->addSql('DROP TABLE respuesta');
        $this->addSql('DROP TABLE solicitud');
        $this->addSql('DROP TABLE usuario');
    }
}
