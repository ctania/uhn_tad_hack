<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EspecialistaRepository")
 */
class Especialista
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombres;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $apellidos;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $tipoIdentificacion;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $identificacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProfesionEspecialista", mappedBy="especialista", orphanRemoval=true)
     */
    private $profesiones;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Idioma", inversedBy="especialistas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idioma;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Respuesta", mappedBy="especialista")
     */
    private $respuestas;

    public function __construct()
    {
        $this->profesiones = new ArrayCollection();
        $this->respuestas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getNombres(): ?string
    {
        return $this->nombres;
    }

    public function setNombres(string $nombres): self
    {
        $this->nombres = $nombres;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getTipoIdentificacion(): ?string
    {
        return $this->tipoIdentificacion;
    }

    public function setTipoIdentificacion(string $tipoIdentificacion): self
    {
        $this->tipoIdentificacion = $tipoIdentificacion;

        return $this;
    }

    public function getIdentificacion(): ?string
    {
        return $this->identificacion;
    }

    public function setIdentificacion(string $identificacion): self
    {
        $this->identificacion = $identificacion;

        return $this;
    }

    /**
     * @return Collection|ProfesionEspecialista[]
     */
    public function getProfesiones(): Collection
    {
        return $this->profesiones;
    }

    public function addProfesione(ProfesionEspecialista $profesione): self
    {
        if (!$this->profesiones->contains($profesione)) {
            $this->profesiones[] = $profesione;
            $profesione->setEspecialista($this);
        }

        return $this;
    }

    public function removeProfesione(ProfesionEspecialista $profesione): self
    {
        if ($this->profesiones->contains($profesione)) {
            $this->profesiones->removeElement($profesione);
            // set the owning side to null (unless already changed)
            if ($profesione->getEspecialista() === $this) {
                $profesione->setEspecialista(null);
            }
        }

        return $this;
    }

    public function getIdioma(): ?Idioma
    {
        return $this->idioma;
    }

    public function setIdioma(?Idioma $idioma): self
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * @return Collection|Respuesta[]
     */
    public function getRespuestas(): Collection
    {
        return $this->respuestas;
    }

    public function addRespuesta(Respuesta $respuesta): self
    {
        if (!$this->respuestas->contains($respuesta)) {
            $this->respuestas[] = $respuesta;
            $respuesta->setEspecialista($this);
        }

        return $this;
    }

    public function removeRespuesta(Respuesta $respuesta): self
    {
        if ($this->respuestas->contains($respuesta)) {
            $this->respuestas->removeElement($respuesta);
            // set the owning side to null (unless already changed)
            if ($respuesta->getEspecialista() === $this) {
                $respuesta->setEspecialista(null);
            }
        }

        return $this;
    }
}
