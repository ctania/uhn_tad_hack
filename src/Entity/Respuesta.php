<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RespuestaRepository")
 */
class Respuesta
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $respuesta;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Especialista", inversedBy="respuestas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $especialista;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Adjunto", mappedBy="respuesta")
     */
    private $adjuntos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Solicitud", inversedBy="respuestas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $solicitud;

    public function __construct()
    {
        $this->adjuntos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRespuesta(): ?string
    {
        return $this->respuesta;
    }

    public function setRespuesta(string $respuesta): self
    {
        $this->respuesta = $respuesta;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getEspecialista(): ?Especialista
    {
        return $this->especialista;
    }

    public function setEspecialista(?Especialista $especialista): self
    {
        $this->especialista = $especialista;

        return $this;
    }

    /**
     * @return Collection|Adjunto[]
     */
    public function getAdjuntos(): Collection
    {
        return $this->adjuntos;
    }

    public function addAdjunto(Adjunto $adjunto): self
    {
        if (!$this->adjuntos->contains($adjunto)) {
            $this->adjuntos[] = $adjunto;
            $adjunto->setRespuesta($this);
        }

        return $this;
    }

    public function removeAdjunto(Adjunto $adjunto): self
    {
        if ($this->adjuntos->contains($adjunto)) {
            $this->adjuntos->removeElement($adjunto);
            // set the owning side to null (unless already changed)
            if ($adjunto->getRespuesta() === $this) {
                $adjunto->setRespuesta(null);
            }
        }

        return $this;
    }

    public function getSolicitud(): ?Solicitud
    {
        return $this->solicitud;
    }

    public function setSolicitud(?Solicitud $solicitud): self
    {
        $this->solicitud = $solicitud;

        return $this;
    }
}
