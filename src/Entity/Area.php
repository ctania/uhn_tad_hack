<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AreaRepository")
 */
class Area
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $codigo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Solicitud", mappedBy="area")
     */
    private $solicitudes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Profesion", inversedBy="areas")
     */
    private $profesiones;

    public function __construct()
    {
        $this->solicitudes = new ArrayCollection();
        $this->profesiones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * @return Collection|Solicitud[]
     */
    public function getSolicitudes(): Collection
    {
        return $this->solicitudes;
    }

    public function addSolicitude(Solicitud $solicitude): self
    {
        if (!$this->solicitudes->contains($solicitude)) {
            $this->solicitudes[] = $solicitude;
            $solicitude->setArea($this);
        }

        return $this;
    }

    public function removeSolicitude(Solicitud $solicitude): self
    {
        if ($this->solicitudes->contains($solicitude)) {
            $this->solicitudes->removeElement($solicitude);
            // set the owning side to null (unless already changed)
            if ($solicitude->getArea() === $this) {
                $solicitude->setArea(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Profesion[]
     */
    public function getProfesiones(): Collection
    {
        return $this->profesiones;
    }

    public function addProfesione(Profesion $profesione): self
    {
        if (!$this->profesiones->contains($profesione)) {
            $this->profesiones[] = $profesione;
        }

        return $this;
    }

    public function removeProfesione(Profesion $profesione): self
    {
        if ($this->profesiones->contains($profesione)) {
            $this->profesiones->removeElement($profesione);
        }

        return $this;
    }
}
