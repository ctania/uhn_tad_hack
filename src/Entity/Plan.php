<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanRepository")
 */
class Plan
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="integer")
     */
    private $maximoSolicitudesSemanales;

    /**
     * @ORM\Column(type="integer")
     */
    private $maximoRespuestasSolicitud;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Usuario", mappedBy="plan")
     */
    private $usuarios;

    public function __construct()
    {
        $this->usuarios = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getMaximoSolicitudesSemanales(): ?int
    {
        return $this->maximoSolicitudesSemanales;
    }

    public function setMaximoSolicitudesSemanales(int $maximoSolicitudesSemanales): self
    {
        $this->maximoSolicitudesSemanales = $maximoSolicitudesSemanales;

        return $this;
    }

    public function getMaximoRespuestasSolicitud(): ?int
    {
        return $this->maximoRespuestasSolicitud;
    }

    public function setMaximoRespuestasSolicitud(int $maximoRespuestasSolicitud): self
    {
        $this->maximoRespuestasSolicitud = $maximoRespuestasSolicitud;

        return $this;
    }

    /**
     * @return Collection|Usuario[]
     */
    public function getUsuarios(): Collection
    {
        return $this->usuarios;
    }

    public function addUsuario(Usuario $usuario): self
    {
        if (!$this->usuarios->contains($usuario)) {
            $this->usuarios[] = $usuario;
            $usuario->setPlan($this);
        }

        return $this;
    }

    public function removeUsuario(Usuario $usuario): self
    {
        if ($this->usuarios->contains($usuario)) {
            $this->usuarios->removeElement($usuario);
            // set the owning side to null (unless already changed)
            if ($usuario->getPlan() === $this) {
                $usuario->setPlan(null);
            }
        }

        return $this;
    }
}
