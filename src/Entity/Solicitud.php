<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SolicitudRepository")
 */
class Solicitud
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $codigo;

    /**
     * @ORM\Column(type="string", length=125)
     */
    private $asunto;

    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="date")
     */
    private $fecha;

    /**
     * @ORM\Column(type="integer")
     */
    private $prioridad;

    /**
     * @ORM\Column(type="boolean")
     */
    private $esActiva;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Idioma", inversedBy="solicitudes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $idioma;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Respuesta", mappedBy="solicitud", orphanRemoval=true)
     */
    private $respuestas;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Adjunto", mappedBy="solicitud")
     */
    private $adjuntos;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Area", inversedBy="solicitudes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $area;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario", inversedBy="solicitudes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    /**
     * @ORM\Column(type="boolean")
     */
    private $respuestaSms;

    public function __construct()
    {
        $this->respuestas = new ArrayCollection();
        $this->adjuntos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    public function setCodigo(string $codigo): self
    {
        $this->codigo = $codigo;

        return $this;
    }

    public function getAsunto(): ?string
    {
        return $this->asunto;
    }

    public function setAsunto(string $asunto): self
    {
        $this->asunto = $asunto;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getPrioridad(): ?int
    {
        return $this->prioridad;
    }

    public function setPrioridad(int $prioridad): self
    {
        $this->prioridad = $prioridad;

        return $this;
    }

    public function getEsActiva(): ?bool
    {
        return $this->esActiva;
    }

    public function setEsActiva(bool $esActiva): self
    {
        $this->esActiva = $esActiva;

        return $this;
    }

    public function getIdioma(): ?Idioma
    {
        return $this->idioma;
    }

    public function setIdioma(?Idioma $idioma): self
    {
        $this->idioma = $idioma;

        return $this;
    }

    /**
     * @return Collection|Respuesta[]
     */
    public function getRespuestas(): Collection
    {
        return $this->respuestas;
    }

    public function addRespuesta(Respuesta $respuesta): self
    {
        if (!$this->respuestas->contains($respuesta)) {
            $this->respuestas[] = $respuesta;
            $respuesta->setSolicitud($this);
        }

        return $this;
    }

    public function removeRespuesta(Respuesta $respuesta): self
    {
        if ($this->respuestas->contains($respuesta)) {
            $this->respuestas->removeElement($respuesta);
            // set the owning side to null (unless already changed)
            if ($respuesta->getSolicitud() === $this) {
                $respuesta->setSolicitud(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Adjunto[]
     */
    public function getAdjuntos(): Collection
    {
        return $this->adjuntos;
    }

    public function addAdjunto(Adjunto $adjunto): self
    {
        if (!$this->adjuntos->contains($adjunto)) {
            $this->adjuntos[] = $adjunto;
            $adjunto->setSolicitud($this);
        }

        return $this;
    }

    public function removeAdjunto(Adjunto $adjunto): self
    {
        if ($this->adjuntos->contains($adjunto)) {
            $this->adjuntos->removeElement($adjunto);
            // set the owning side to null (unless already changed)
            if ($adjunto->getSolicitud() === $this) {
                $adjunto->setSolicitud(null);
            }
        }

        return $this;
    }

    public function getArea(): ?Area
    {
        return $this->area;
    }

    public function setArea(?Area $area): self
    {
        $this->area = $area;

        return $this;
    }

    public function getUsuario(): ?Usuario
    {
        return $this->usuario;
    }

    public function setUsuario(?Usuario $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getRespuestaSms(): ?bool
    {
        return $this->respuestaSms;
    }

    public function setRespuestaSms(bool $respuestaSms): self
    {
        $this->respuestaSms = $respuestaSms;

        return $this;
    }
}
