<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProfesionEspecialistaRepository")
 */
class ProfesionEspecialista
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numeroTarjetaProfesional;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $universidad;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tituloUniversitario;

    /**
     * @ORM\Column(type="date")
     */
    private $fechaGrado;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Especialista", inversedBy="profesiones")
     * @ORM\JoinColumn(nullable=false)
     */
    private $especialista;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Profesion", inversedBy="especialistas")
     * @ORM\JoinColumn(nullable=false)
     */
    private $profesion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumeroTarjetaProfesional(): ?string
    {
        return $this->numeroTarjetaProfesional;
    }

    public function setNumeroTarjetaProfesional(string $numeroTarjetaProfesional): self
    {
        $this->numeroTarjetaProfesional = $numeroTarjetaProfesional;

        return $this;
    }

    public function getUniversidad(): ?string
    {
        return $this->universidad;
    }

    public function setUniversidad(string $universidad): self
    {
        $this->universidad = $universidad;

        return $this;
    }

    public function getTituloUniversitario(): ?string
    {
        return $this->tituloUniversitario;
    }

    public function setTituloUniversitario(string $tituloUniversitario): self
    {
        $this->tituloUniversitario = $tituloUniversitario;

        return $this;
    }

    public function getFechaGrado(): ?\DateTimeInterface
    {
        return $this->fechaGrado;
    }

    public function setFechaGrado(\DateTimeInterface $fechaGrado): self
    {
        $this->fechaGrado = $fechaGrado;

        return $this;
    }

    public function getEspecialista(): ?Especialista
    {
        return $this->especialista;
    }

    public function setEspecialista(?Especialista $especialista): self
    {
        $this->especialista = $especialista;

        return $this;
    }

    public function getProfesion(): ?Profesion
    {
        return $this->profesion;
    }

    public function setProfesion(?Profesion $profesion): self
    {
        $this->profesion = $profesion;

        return $this;
    }
}
