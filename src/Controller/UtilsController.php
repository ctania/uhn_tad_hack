<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Area;
use App\Entity\Especialista;
use App\Entity\Idioma;
use App\Entity\Plan;
use App\Entity\Profesion;
use App\Entity\ProfesionEspecialista;
use App\Entity\Usuario;
use Symfony\Component\Validator\Constraints\IsNull;

class UtilsController extends AbstractController
{
    /**
     * @Route("/utils", name="utils")
     */
    public function index()
    {
        
        return $this->render('utils/index.html.twig', [
            'controller_name' => 'UtilsController',
        ]);
    }

    
    /**
     * Esta función se encarga de generar y almacenar en base de datos la información 
     * de prueba de la aplicación.
     * @Route("/utils/cargarDatosPrueba", name="cargarDatosPrueba")
     */
    public function cargarDatosPrueba() {

        $this->cargarIdiomas();
        $this->cargarAreas();
        $this->cargarProfesiones();
        $this->cargarEspecialistas();
        $this->cargarProfesionEspecialistas();
        $this->cargarPlanes();
        $this->cargarUsuarios();

        die;

        return $this->render('utils/index.html.twig', [
            'controller_name' => 'UtilsController',
        ]);
    }


    /**
     * Esta función se encarga de generar y almacenar en base de datos la información 
     * de prueba de la aplicación.
     * @Route("/utils/cargarAreas", name="cargarAreas")
     */
    public function cargarAreas() {

        // Obtener el entity manager
        $entityManager = $this->getDoctrine()->getManager();

        // Áreas de prueba a crear
        $areas = [
            "Medicina",
            "Medicina Veterinaria",
            "Asesoría Jurídica",
            "Asesoría Académica",
            "Tecnología"
        ];

        // Creación de áreas de prueba
        for ($i=0; $i< sizeof($areas); $i++) {
            
            $area = new Area();
            $area->setCodigo("COD_".($i+1));
            $area->setNombre($areas[$i]);
            $entityManager->persist($area);
            $entityManager->flush();
        }

        print("<h5>Áreas creadas exitosamente<h5>");
        

    }

    /**
     * Esta función se encarga de generar y almacenar en base de datos la información 
     * de prueba de la aplicación.
     * @Route("/utils/cargarProfesiones", name="cargarProfesiones")
     */
    public function cargarProfesiones() {

        // Obtener el entity manager
        $entityManager = $this->getDoctrine()->getManager();

        // Profesiones de prueba a crear [area => profesion]
        $profesiones = [
            "Medicina" => ["Medico General", "Enfermero"],
            "Medicina Veterinaria" => ["Medico Veterinario", "Técnico veterinario"],
            "Asesoría Jurídica" => ["Abogado"],
            "Asesoría Académica" => ["Docente", "Estudiante"],
            "Tecnología" => ["Ingeniero de Sistemas", "Ingeniero Electrónico"]
        ];

        // Creación de profesiones de prueba
        foreach ($profesiones as $key => $profesionArea) {

            $area = $entityManager->getRepository(Area::class)->findOneBy(array('nombre' => $key));
            
            if(!isset($area)){

                print("<br><h5>Area ".$key." no encontrada. <h5>");

                return $this->render('utils/index.html.twig', [
                    'controller_name' => 'UtilsController',
                ]);
            }

            foreach ($profesionArea as $profesion) {
            
                $objProfesion = new Profesion();
                $objProfesion->setNombre($profesion);
                $objProfesion->addArea($area);
                $entityManager->persist($objProfesion);
                $entityManager->flush();
            }        
        }
    
        print("<br><h5>Profesiones creadas exitosamente<h5>");

    }


    /**
     * Esta función se encarga de generar y almacenar en base de datos la información 
     * de prueba de la aplicación.
     * @Route("/utils/cargarIdiomas", name="cargarIdiomas")
     */
    public function cargarIdiomas() {

        // Obtener el entity manager
        $entityManager = $this->getDoctrine()->getManager();

        // Áreas de prueba a crear
        $idiomas = [
            "es" => "Español",
            "en" => "Inglés",
            "fr" => "Francés"
        ];

        // Creación de áreas de prueba
        foreach ($idiomas as $codigo => $valor) {
            
            $idioma = new Idioma();
            $idioma->setCodigo($codigo);
            $idioma->setNombre($valor);
            $entityManager->persist($idioma);
            $entityManager->flush();
        }

        print("<h5>Idiomas creados exitosamente.<h5>");
        
    }


    /**
     * Esta función se encarga de generar y almacenar en base de datos la información 
     * de prueba de la aplicación.
     * @Route("/utils/cargarEspecialistas", name="cargarEspecialistas")
     */
    public function cargarEspecialistas() {

        // Obtener el entity manager
        $entityManager = $this->getDoctrine()->getManager();
        $telefono = "573154178841";

        // Idiomas que se pueden cargar
        $idiomaEspañol = $entityManager->getRepository(Idioma::class)->findOneBy(array('codigo' => 'es'));
        $idiomaIngles = $entityManager->getRepository(Idioma::class)->findOneBy(array('codigo' => 'en'));

        if(is_null($idiomaEspañol) || is_null($idiomaIngles))  {
            
            print("<h5>Hay error<h5>");
            die;
        }

        // Especialista Jhonatan - Español
        $objEspecialista = new Especialista();
        $objEspecialista->setTelefono($telefono);
        $objEspecialista->setIdioma($idiomaEspañol);
        $objEspecialista->setNombres("Jonathan");
        $objEspecialista->setApellidos("Ibarra");
        $objEspecialista->setTipoIdentificacion("CC");
        $objEspecialista->setIdentificacion("106111222");
        $entityManager->persist($objEspecialista);
        $entityManager->flush();

        // Especialista Cristian - Inglés
        $objEspecialista = new Especialista();
        $objEspecialista->setTelefono($telefono);
        $objEspecialista->setIdioma($idiomaEspañol);
        $objEspecialista->setNombres("Cristian");
        $objEspecialista->setApellidos("Gomez");
        $objEspecialista->setTipoIdentificacion("CC");
        $objEspecialista->setIdentificacion("1061333444");
        $entityManager->persist($objEspecialista);
        $entityManager->flush();

        print("<h5>Especialistas creados exitosamente.<h5>");

    }


    /**
     * Esta función se encarga de generar y almacenar en base de datos la información 
     * de prueba de la aplicación.
     * @Route("/utils/cargarProfesionEspecialistas", name="cargarProfesionEspecialistas")
     */
    public function cargarProfesionEspecialistas() {

        // Obtener el entity manager
        $entityManager = $this->getDoctrine()->getManager();
        $telefono = "573154178841";

        // Profesiones a cargar
        $medicoGeneral = $entityManager->getRepository(Profesion::class)->findOneBy(array('nombre' => 'Medico General'));
        $medicoVeterinario = $entityManager->getRepository(Profesion::class)->findOneBy(array('nombre' => 'Medico Veterinario'));

        // Especialistas a cargar
        $objEspecialistaJonathan = $entityManager->getRepository(Especialista::class)->findOneBy(array('identificacion' => '106111222'));
        $objEspecialistaCristian = $entityManager->getRepository(Especialista::class)->findOneBy(array('identificacion' => '1061333444'));

        // Creacion de las relaciones especialista profesion
        $objProfesionEspecialista = new ProfesionEspecialista;
        $objProfesionEspecialista->setProfesion($medicoGeneral);
        $objProfesionEspecialista->setEspecialista($objEspecialistaJonathan);
        $objProfesionEspecialista->setNumeroTarjetaProfesional("2323423556-53");
        $objProfesionEspecialista->setUniversidad("Universidad del Cauca");
        $objProfesionEspecialista->setTituloUniversitario("Médico General");
        $objProfesionEspecialista->setFechaGrado(new \DateTime('@'.strtotime('now')));
        $entityManager->persist($objProfesionEspecialista);
        $entityManager->flush();

        // Creacion de las relaciones especialista profesion
        $objProfesionEspecialista = new ProfesionEspecialista;
        $objProfesionEspecialista->setProfesion($medicoVeterinario);
        $objProfesionEspecialista->setEspecialista($objEspecialistaCristian);
        $objProfesionEspecialista->setNumeroTarjetaProfesional("2323423556-53");
        $objProfesionEspecialista->setUniversidad("Universidad del Cauca");
        $objProfesionEspecialista->setTituloUniversitario("Médico Veterinario");
        $objProfesionEspecialista->setFechaGrado(new \DateTime('@'.strtotime('now')));
        $entityManager->persist($objProfesionEspecialista);
        $entityManager->flush();

        print("<h5>Relaciones profesion especialista creadas exitosamente.<h5>");
    }


    /**
     * Esta función se encarga de generar y almacenar en base de datos la información 
     * de prueba de la aplicación.
     * @Route("/utils/cargarPlanes", name="cargarPlanes")
     */
    public function cargarPlanes() {

        // Obtener el entity manager
        $entityManager = $this->getDoctrine()->getManager();
        
        // Plan gratuito
        $objPlan = new Plan();
        $objPlan->setNombre("Plan Free");
        $objPlan->setMaximoRespuestasSolicitud(5);
        $objPlan->setMaximoSolicitudesSemanales(2);
        $entityManager->persist($objPlan);
        $entityManager->flush();

        // Plan Premium
        $objPlan = new Plan();
        $objPlan->setNombre("Plan Premium");
        $objPlan->setMaximoRespuestasSolicitud(40);
        $objPlan->setMaximoSolicitudesSemanales(20);
        $entityManager->persist($objPlan);
        $entityManager->flush();

        print("<h5>Planes creados exitosamente.<h5>");
    }    


    /**
     * Esta función se encarga de generar y almacenar en base de datos la información 
     * de prueba de la aplicación.
     * @Route("/utils/cargarUsuarios", name="cargarUsuarios")
     */
    public function cargarUsuarios() {

        // Obtener el entity manager
        $entityManager = $this->getDoctrine()->getManager();
        $telefono = "573017095084";

        // Planes que se pueden cargar
        $objPlanFree = $entityManager->getRepository(Plan::class)->findOneBy(array('nombre' => 'Plan Free'));
        $objPlanPremium = $entityManager->getRepository(Plan::class)->findOneBy(array('nombre' => 'Plan Premium'));

        if(is_null($objPlanFree) || is_null($objPlanPremium))  {
            
            print("<h5>Hay error en usuario<h5>");
            die;
        }

        // Usuario Stiven - Premium Plan
        $objUsuario = new Usuario();
        $objUsuario->setPlan($objPlanPremium);
        $objUsuario->setTelefono($telefono);
        $objUsuario->setNombres("Stiven");
        $objUsuario->setApellidos("Mamián");
        $objUsuario->setTipoIdentificacion("CC");
        $objUsuario->setIdentificacion("1061999000");
        $entityManager->persist($objUsuario);
        $entityManager->flush();

        // Usuario Tania - Free Plan
        $objUsuario = new Usuario();
        $objUsuario->setPlan($objPlanPremium);
        $objUsuario->setTelefono($telefono);
        $objUsuario->setNombres("Tania");
        $objUsuario->setApellidos("Cañizares");
        $objUsuario->setTipoIdentificacion("CC");
        $objUsuario->setIdentificacion("1061888999");
        $entityManager->persist($objUsuario);
        $entityManager->flush();

        print("<h5>Usuarios creados exitosamente.<h5>");
    }    

}
