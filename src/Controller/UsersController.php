<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UsersController extends AbstractController
{
    /**
     * @Route("/users", name="users")
     */
    public function index()
    {
        return $this->render('users/index.html.twig', [
            'controller_name' => 'UsersController',
        ]);
    }

    /**
     * @Route("/usuario/registro", name="usuario_registro")
     */
    public function registroUsuario()
    {
        return $this->render('users/registro_usuario.html.twig', [
            'controller_name' => 'UsersController',
        ]);
    }

    /**
     * @Route("/usuario/home", name="usuario_home")
     */
    public function homeUsuario()
    {
        return $this->render('users/home_usuario.html.twig', [
            'controller_name' => 'UsersController',
        ]);
    }

    /**
     * @Route("/especialista/home", name="especialista_home")
     */
    public function homeEspecialista()
    {
        return $this->render('users/home_especialista.html.twig', [
            'controller_name' => 'UsersController',
        ]);
    }
}
