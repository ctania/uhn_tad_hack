<?php

namespace App\Controller;

use telesign\sdk\messaging\MessagingClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\Solicitud;
use App\Entity\Respuesta;
use App\Entity\Especialista;

class RespuestasController extends AbstractController
{
    /**
     * @Route("/respuestas", name="respuestas")
     */
    public function index()
    {
        return $this->render('respuestas/index.html.twig', [
            'controller_name' => 'RespuestasController',
        ]);
    }

    /**
     * @Route("/usuario/respuestas/{idSolicitud}", name="ver_respuestas")
     */
    public function buscarRespuestas($idSolicitud)
    {
        //Se obtienen todas las respuestas a una solicitud de usuario
        $solicitud = $this->getDoctrine()->getRepository(Solicitud::class)->find($idSolicitud);
        if(!is_null($solicitud)){
            //Si existe la respuesta
            $respuestas = $solicitud->getRespuestas();
            return $this->render('respuestas/usuario_ver_respuestas.html.twig', [
                'solicitud' => $solicitud,
                'respuestas' => $respuestas,
            ]);
        }else{
        
            //Si no existe la solicitud se muestra mensaje de error
        } 
    }

    /**
     * @Route("/especialista/solicitud/responder/{idSolicitud}", name="especialista_responder_solicitud")
     */
    public function especialistaResponderSolicitud($idSolicitud, Request $request)
    {
        //Se establece el codigo en la variable de sesion
        $session = new Session();

        //Si no esta iniciada la sesión se inicia
        if(!isset($_SESSION)){
            $session->start();
        }

        //Se obtiene la respuesta
        $respuesta = $request->get('respuesta');

        //Se obtiene el usuario de la sesion
        $especialistaActual = $this->getDoctrine()->getRepository(Especialista::class)->find($session->get('idUsuario'));
        $esEspecialista = $session->get('esEspecialista');
        
        //Se valida que el usuario exista y sea un usuario normal
        if(!is_null($especialistaActual) && !is_null($esEspecialista) && $esEspecialista){

            //Se valida que la respuesta sea valida
            if(strcmp("", $respuesta) === 0){

                //Se retorna a una vista de error
                return $this->render('respuestas/error.html.twig', [
                    'titulo_error' => 'Error',
                    'mensaje_error' => "The reply cannot be empty",
                ]);
            }

            //Se obtiene la solicitud asociada 
            $solicitudAsociada = $this->getDoctrine()->getRepository(Solicitud::class)->find($idSolicitud);

            //Se valida que la solitud exista
            if(is_null($solicitudAsociada)){

                //Se retorna a una vista de error
                return $this->render('respuestas/error.html.twig', [
                    'titulo_error' => 'Request not found',
                    'mensaje_error' => "The request that you're trying reply doesn't exists",
                ]);
            }

            try{

                //Se obtiene la instancia del manager de entidades
                $entityManager = $this->getDoctrine()->getManager();

                //Se genera la nueva respuesta
                $objRespuesta = new Respuesta();
                $objRespuesta->setRespuesta($respuesta);
                $objRespuesta->setFecha(new \DateTime('@'.strtotime('now')));
                $objRespuesta->setSolicitud($solicitudAsociada);
                $objRespuesta->setEspecialista($especialistaActual);
                $entityManager->persist($objRespuesta);        

                //Se asocia la respuesta a la solicitud
                $solicitudAsociada->addRespuesta($objRespuesta);
                $especialistaActual->addRespuesta($objRespuesta);
                $entityManager->persist($solicitudAsociada);
                $entityManager->persist($especialistaActual);
                $entityManager->flush();

                //Se envia la notificacion al usuario (guardando el error en caso de hacerlo)
                $errores = "";
                if(!$this->enviarNotificacion($solicitudAsociada, $objRespuesta)){
                    $errores = "<br><br>An error ocurred trying to send the notification message";
                }

                //Se retorna a una vista de exito
                return $this->render('respuestas/correcto.html.twig', [
                    'titulo_mensaje' => 'Your Reply Have Been Sent',
                    'mensaje' => "Thanks for your contribution".$errores,
                ]);

            } catch (\Exception $e){

                //Se retorna a una vista de error
                return $this->render('respuestas/error.html.twig', [
                    'titulo_error' => 'Operation failed',
                    'mensaje_error' => $e->getMessage(),
                ]);
            }
            
        } else {

            //Se retorna a una vista de error
            return $this->render('respuestas/error.html.twig', [
                'titulo_error' => 'Forbidden',
                'mensaje_error' => "You don't have permission to access this feature",
            ]);
        }
    }

    //=================================================
    //=================================================

    /**
     * Función que envía un mensaje con la respuesta o con la notificación
     * de una solicitud
     */
    private function enviarNotificacion(Solicitud $solicitud, Respuesta $respuesta){

         /**
         * Obtener el usuario que envia la solicitud
         */
        $usuario = $solicitud->getUsuario();

        //Se obtiene el numero telefonico del usuario
        $phone_number = $usuario->getTelefono();

        //Obtener los parametros de la conexion con telesign
        $customer_id = $this->getParameter('telesign_customer_id');
        $api_key = $this->getParameter('telesign_api_key');
        $message_type = "ARN";

        //Se genera el mensaje de acuerdo a la configuracion de la notificacion
        $message = "";

        //Se valida si se activo la respuesta completa por sms
        if($solicitud->getRespuestaSms()){

            $message = "UHN Notification service\n\n".
                "Your request '".$solicitud->getAsunto()."' has a new reply from ".
                $respuesta->getEspecialista()->getNombres()." ".
                $respuesta->getEspecialista()->getApellidos()."\n\n".
                "Reply:\n".
                $respuesta->getRespuesta()."\n".
                "Date: ".$respuesta->getFecha()->format('d-m-Y')."\n\n".
                "See full requests on UHN, request code ".$solicitud->getCodigo();
        } else {

            $message = "UHN Notification service\n\n".
                "Your request '".$solicitud->getAsunto()."' has a new reply from ".
                $respuesta->getEspecialista()->getNombres()." ".
                $respuesta->getEspecialista()->getApellidos()."\n\n".
                "See request on UHN, request code ".$solicitud->getCodigo();
        }
        
        //Se envia el mensaje al cliente
        $messaging = new MessagingClient($customer_id, $api_key);
        $response = $messaging->message($phone_number, $message, $message_type);

        //Se valida el resultado de la transaccion
        if($response->status_code !== 200){

            //Si el resultado es incorrecto se retorna false
            return false;
        }

        //Se retorna la respuesta
        return true;
    }
}
