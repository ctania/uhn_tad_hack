<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\Solicitud;
use App\Entity\Area;
use App\Entity\Idioma;
use App\Entity\Usuario;

class SolicitudesController extends AbstractController
{
    /**
     * @Route("/usuario/solicitud/nueva", name="usuario_nueva_solicitud")
     */
    public function index()
    {
        return $this->render('solicitudes/radicar_solicitud.html.twig', [
            'controller_name' => 'SolicitudesController',
        ]);
    }

    /**
     * @Route("/solicitudes/ver", name="solicitudes_ver")
     */
    public function verSolicitudes()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $solicitudes = $entityManager->getRepository(Solicitud::class)->findAll();

        return $this->render('solicitudes/especialista_lista_solicitud.html.twig', [
            'solicitudes' => $solicitudes,
        ]);

    }

    /**
     * @Route("/solicitudes/verSolicitud/{idSolicitud}", name="ver_detalle_solicitud")
     */
    public function verDetalleSolicitud($idSolicitud)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $solicitud = $entityManager->getRepository(Solicitud::class)->find($idSolicitud);

        return $this->render('solicitudes/especialista_verDetalle_solicitud.html.twig', [
            'solicitud' => $solicitud,
        ]);
    }

    

    /**
     * @Route("/usuario/solicitudes/listar", name="usuario_solicitudes_listar")
     */
    public function usuarioListarSolicitudes()
    {

        //Se obtienen todas las solicitudes del usuario
        $solicitudesUsuario = $this->getDoctrine()->getRepository(Solicitud::class)->findAll();

        return $this->render('solicitudes/usuario_respuesta_listar.html.twig', [
            'solicitudes' => $solicitudesUsuario,
        ]);
    }

    /**
     * @Route("/usuario/solicitudes/buscar", name="usuario_solicitudes_buscar")
     */
    public function buscarSolicitudesUsuario()
    {

        //Se obtienen todas las solicitudes del usuario
        $solicitudesUsuario = $this->getDoctrine()->getRepository(Solicitud::class)->findAll();

        return $this->render('solicitudes/usuario_respuesta_buscar.html.twig', [
            'solicitudes' => $solicitudesUsuario,
        ]);
    }

    /**
     * @Route("/usuario/solicitudes/buscar/action", name="usuario_solicitudes_buscar_codigo")
     */
    public function usuarioBuscarSolicitudCodigo(Request $request)
    {
        //Se establece el codigo en la variable de sesion
        $session = new Session();

        //Si no esta iniciada la sesión se inicia
        if(!isset($_SESSION)){
            $session->start();
        }

        //Se obtiene el codigo desde la solicitud
        $codigoSolicitud = $request->get('requestCode');

        //Se obtiene el usuario de la sesion
        $usuarioActual = $session->get('objUsuario');
        $esEspecialista = $session->get('esEspecialista');

        //Se valida que el usuario exista y sea un usuario normal
        if(!is_null($usuarioActual) && !is_null($esEspecialista) && !$esEspecialista){

            //Se obtienen todas las solicitudes del usuario
            $solicitudUsuario = $this->getDoctrine()->getRepository(Solicitud::class)->findOneBy(array(
                "codigo" => $codigoSolicitud,
                "usuario" => $usuarioActual
            ));

            //Se valida si la solicitud existe
            if(!is_null($solicitudUsuario)){

                //Se retorna a una vista de error
                return $this->redirectToRoute('ver_respuestas', array(
                    "idSolicitud" => $solicitudUsuario->getId()
                ));
            }else{

                //Se retorna a una vista de error
                return $this->render('solicitudes/error.html.twig', [
                    'titulo_error' => 'Not found',
                    'mensaje_error' => "There are no Request with the code ".$codigoSolicitud,
                ]);
            }
        } else {

            //Se retorna a una vista de error
            return $this->render('solicitudes/error.html.twig', [
                'titulo_error' => 'Forbidden',
                'mensaje_error' => "You don't have permission to access this feature",
            ]);
        }
    }


    /**
     * Esta función contiene el código que permite radicar una solicitud.
     * @Route("/usuario/solicitud/radicar", name="usuario_radicar_solicitud")
     */
    public function radicarSolicitud(Request $request)
    {
        // crear el entity manager
        $entityManager = $this->getDoctrine()->getManager();

        //Se establece el codigo en la variable de sesion
        $session = new Session();

        //Si no esta iniciada la sesión se inicia
        if(!isset($_SESSION)){
            $session->start();
        }

        // Se obtienen los datos de la solicitud
        $codigoArea = $request->get('area');
        $codigoIdioma = $request->get('idioma');
        $asunto = $request->get('asunto');
        $descripcion = $request->get('descripcion');
        $respuestaSMS = $request->get('respuestaSMS');
        
        echo "<br>".$codigoArea;
        echo "<br>".$codigoIdioma;
        echo "<br>".$asunto;
        echo "<br>".$descripcion;
        echo "<br>".$respuestaSMS;


        //Se obtiene el usuario de la sesion
        $usuarioActual = $entityManager->getRepository(Usuario::class)->find($session->get('idUsuario'));
        $esEspecialista = $session->get('esEspecialista');

        //Se valida que el usuario exista y sea un usuario normal
        if(!is_null($usuarioActual) && !is_null($esEspecialista) && !$esEspecialista){

            // obtener el área escogida
            $area = $entityManager->getRepository(Area::class)->findOneBy(array('codigo' => $codigoArea));

            // obtener el idioma
            $idioma = $entityManager->getRepository(Idioma::class)->findOneBy(array('codigo' => $codigoIdioma));
            
            if($area == null || $idioma == null) {
                
                //Se retorna a una vista de error
                return $this->render('solicitudes/error.html.twig', [
                    'titulo_error' => 'Error 505s',
                    'mensaje_error' => "Error en el registro. ERROR 505 en servidor. No se encuentra el area/idioma asociados.",
                ]);
            }

            // se arma la solicitud
            $objSolicitud = new Solicitud();
            $objSolicitud->setArea($area);
            $objSolicitud->setIdioma($idioma);
            $objSolicitud->setAsunto($asunto);
            $objSolicitud->setDescripcion($descripcion);
            $objSolicitud->setRespuestaSms($respuestaSMS);
            $objSolicitud->setUsuario($usuarioActual);
            $objSolicitud->setFecha(new \DateTime('@'.strtotime('now')));
            $objSolicitud->setEsActiva(true);
            $objSolicitud->setCodigo($area->getCodigo()."_".uniqid());
            //TODO: Vericar la prioridad con su plan asociado y adjuntos 
            $objSolicitud->setPrioridad(1);
            $entityManager->persist($objSolicitud);
            $entityManager->flush();

            // retorna a la vista de listar
            return $this->redirectToRoute('usuario_solicitudes_listar');

        } else {

            //Se retorna a una vista de error
            return $this->render('solicitudes/error.html.twig', [
                'titulo_error' => 'Forbidden',
                'mensaje_error' => "You don't have permission to access this feature",
            ]);
        }

    }

}
