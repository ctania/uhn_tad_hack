<?php

namespace App\Controller;

use function telesign\sdk\util\randomWithNDigits;
use telesign\sdk\messaging\MessagingClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\Usuario;
use App\Entity\Especialista;

class LoginController extends AbstractController
{

    /**
     * @Route("/", name="login")
     */
    public function index()
    {
        return $this->render('login/login.html.twig', [
            'controller_name' => 'LoginController',
        ]);
    }

    /**
     * @Route("/verificar", name="verificar")
     */
    public function verificar()
    {
        return $this->render('login/verificar.html.twig', [
            'controller_name' => 'LoginController',
        ]);
    }

    /**
     * @Route("/error", name="error")
     */
    public function Error()
    {
        return $this->render('login/error.html.twig', [
            'controller_name' => 'LoginController',
        ]);
    }

    /**
     * Función que carga la vista principal para inicio a la plataforma
     * 
     * @Route("/login/autenticar", name="login_autenticar")
     */
    public function autenticarUsuario(Request $request)
    {

        /**
         * Obtener el telefono y pais del usuario
         */
        $pais = $request->get('pais');
        $telefono = $request->get('telefono');

        //Se genera el numero de envio
        $phone_number = $pais.$telefono;

        //Obtener los parametros de la conexion con telesign
        $customer_id = $this->getParameter('telesign_customer_id');
        $api_key = $this->getParameter('telesign_api_key');

        //Se genera el codigo de verificacion
        $verify_code = randomWithNDigits(5);

        //Se genera el objeto de tipo mensaje
        $message_type = "OTP";
        $message = "UHN Autentication Service\n\nYour verify code is ".$verify_code;
        $messaging = new MessagingClient($customer_id, $api_key);

        //Se envia el mensaje al usuario
        $response = $messaging->message($phone_number, $message, $message_type);

        //Se valida el resultado de la transaccion
        if($response->status_code !== 200){

            //Se retorna a la vista de error
            return $this->render('login/error.html.twig', [
                'mensaje_error' => 'Ocurrió un error enviando el mensaje ',
            ]);
        }

        //Se establece el codigo en la variable de sesion
        $session = new Session();

        //Si no esta iniciada la sesión se inicia
        if(!isset($_SESSION)){
            $session->start();
        }

        $session->set('current_code', $verify_code);
        $session->set('phone_number', $phone_number);

        return $this->render('login/verificar.html.twig', [
            'phone_number' => $phone_number,
        ]);
    }

    /**
     * Función que carga la vista principal para inicio a la plataforma
     * 
     * @Route("/login/verificar/codigo", name="login_verificar_codigo")
     */
    public function verificarCodigo(Request $request)
    {

        //Se obtiene la instancia de entity manager
        $entityManager = $this->getDoctrine()->getManager();

        //Se genera una variabla de sesión
        $session = new Session();

        //Si no esta iniciada la sesión se inicia
        if(!isset($_SESSION)){
            $session->start();
        }
        
        //Se obtiene el codigo de verificacion de la sesion
        $codigoGenerado = $session->get('current_code');

        //Se obtiene el codigo enviado por el usuario
        $codigoUsuario = $request->get('codigo');

        //Se comparan los codigos
        if($codigoGenerado === $codigoUsuario){

            //Se obtiene el telefono asociado a la sesion
            $telefono = $session->get('phone_number');

            //Se eliminan las variables de sesion
            $session->remove('current_code');
            $session->remove('phone_number');

            //Se verifica si hay un usuario asociado al numero que acaba de iniciar sesion
            $usuarioAsociado = $this->getDoctrine()->getRepository(Usuario::class)->findOneBy(array(
                'telefono' => $telefono
            ));

            //Se verifica si hay un especialista asociado al numero que acaba de iniciar sesion
            $especialistaAsociado = $this->getDoctrine()->getRepository(Especialista::class)->findOneBy(array(
                'telefono' => $telefono
            ));

            //Se valida si se encontro un usuario
            if(!is_null($usuarioAsociado)){

                //Se asocia en la sesion como usuario
                $session->set('esEspecialista', false);
                $session->set('objUsuario', $usuarioAsociado);
                $session->set('idUsuario', $usuarioAsociado->getId());

                //Se redirige hacia la vista principal de usuario
                return $this->redirectToRoute('usuario_nueva_solicitud');
                
            //Si el usuario es de tipo especialista...
            } else if (!is_null($especialistaAsociado)){

                //Se asocia en la sesion como usuario
                $session->set('esEspecialista', true);
                $session->set('objUsuario', $especialistaAsociado);
                $session->set('idUsuario', $especialistaAsociado->getId());

                //Se redirige hacia la vista principal de especialista
                return $this->redirectToRoute('solicitudes_ver');
            } else {

                //Si no hay ningún registro asociado se redirige a la vista de datos...
                
                //Si no se encuentra redirige a la vista de completar datos
                return $this->redirectToRoute('usuario_registro');
            }
        }else{

            //Se retorna a la vista de error
            return $this->render('login/error.html.twig', [
                'mensaje_error' => 'The code given doesn´t match ',
            ]);
        }
    }

    //=======================================================================
    //      FUNCIONES ÚTILES DE LA CLASE
    //=======================================================================

}
